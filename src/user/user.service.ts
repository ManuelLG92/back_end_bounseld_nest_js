import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { PrismaService } from '../prisma/prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class UserService {
  constructor(private prismaService: PrismaService) {}

  async findAllFromMain() {
    return await this.prismaService.user.findMany({
      include: {
        country: true,
        learningLanguages: {
          include: {
            language: true,
          },
        },
        nativeLanguages: {
          include: {
            language: true,
          },
        },
      },
    });
  }

  async findOneFromMain(id: number) {
    return await this.prismaService.user.findUnique({
      where: { id },
      include: {
        country: true,
        learningLanguages: {
          include: {
            language: true,
          },
        },
        nativeLanguages: {
          include: {
            language: true,
          },
        },
      },
    });
  }

  async updateFromMain(id: number, updateUserInput: UpdateUserInput) {
    let user = await this.prismaService.user.findUnique({
      where: { id },
    });
    // console.log(user);
    if (!user) throw NotFoundException;

    const userPrisma: Prisma.UserUpdateInput = {};

    Object.assign(userPrisma, updateUserInput);

    user = await this.prismaService.user.update({
      where: { id },
      data: userPrisma,
    });
    return user;
  }

  async createFromMain(createUserInput: CreateUserInput) {
    const userData: Prisma.UserCreateInput = {
      name: createUserInput.name,
      surname: createUserInput.surname,
      email: createUserInput.email,
      password: createUserInput.password,
      gender: createUserInput.gender,
      age: createUserInput.age,
      country: {
        connect: {
          id: createUserInput.countryId,
        },
      },
      description: '',
    };

    return await this.prismaService.user.create({
      data: userData,
    });
  }

  async removeFromMain(id: number) {
    try {
      const nativeLanguage = this.prismaService.nativeLanguage.deleteMany({
        where: { userId: id },
      });

      const learningLanguage = this.prismaService.learningLanguage.deleteMany({
        where: { userId: id },
      });

      const report = this.prismaService.report.deleteMany({
        where: { userId: id },
      });

      const user = this.prismaService.user.delete({
        where: { id: id },
      });

      await this.prismaService.$transaction([
        nativeLanguage,
        learningLanguage,
        report,
        user,
      ]);
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}
