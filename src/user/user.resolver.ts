import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User } from './entities/user.entity';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { Inject, OnModuleInit } from '@nestjs/common';
import { ClientKafka, MessagePattern, Payload } from '@nestjs/microservices';
import { Producer } from '@nestjs/microservices/external/kafka.interface';

@Resolver(() => User)
export class UserResolver implements OnModuleInit {
  private kafkaProducer: Producer;

  constructor(
    @Inject('KAFKA_BROKER')
    private clientKafka: ClientKafka,

    private readonly userService: UserService,
  ) {}

  async onModuleInit() {
    this.kafkaProducer = await this.clientKafka.connect();
    this.clientKafka.subscribeToResponseOf('email_confirmation_received');
  }

  @Mutation(() => User)
  async createUserTestBranch(
    @Args('createUserInput') createUserInput: CreateUserInput,
  ) {
    const user = await this.userService.createFromMain(createUserInput);
    if (user) {
      await this.kafkaProducer.send({
        topic: 'send_email',
        messages: [
          {
            key: Math.random() + '',
            value: JSON.stringify({ userId: user.id, email: user.email }),
          },
        ],
      });
      console.log('Se envia user.resolver send_mail');
    }
    return user;
  }

  @Query(() => [User], { name: 'users' })
  findAllTestBranch() {
    return this.userService.findAllFromMain();
  }

  @Query(() => User, { name: 'user' })
  findOneTestBranch(@Args('id', { type: () => Int }) id: number) {
    return this.userService.findOneFromMain(id);
  }

  @Mutation(() => User)
  updateUserTestBranch(@Args('updateUser') updateUserInput: UpdateUserInput) {
    return this.userService.updateFromMain(updateUserInput.id, updateUserInput);
  }

  @Mutation(() => Boolean)
  removeUserTestBranch(@Args('id', { type: () => Int }) id: number) {
    return this.userService.removeFromMain(id);
  }

  @MessagePattern('email_confirmation_received')
  sendEmailTestBranchFromMaster(@Payload() message) {
    console.log('Se recibe user.resolver email_confirmation_received');
    console.log(message.value);
    return {
      reply: 'ok',
    };
  }
}
