import { Record } from '../../records/entity/records.entity';
export declare class User {
    id: number;
    name: string;
    records: Record[];
}
