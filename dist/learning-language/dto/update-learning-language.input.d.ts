import { CreateLearningLanguageInput } from './create-learning-language.input';
declare const UpdateLearningLanguageInput_base: import("@nestjs/common").Type<Partial<CreateLearningLanguageInput>>;
export declare class UpdateLearningLanguageInput extends UpdateLearningLanguageInput_base {
    id: number;
}
export {};
