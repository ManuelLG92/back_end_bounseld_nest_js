import { CreateLanguageInput } from './create-language.input';
declare const UpdateLanguageInput_base: import("@nestjs/common").Type<Partial<CreateLanguageInput>>;
export declare class UpdateLanguageInput extends UpdateLanguageInput_base {
    id: number;
}
export {};
