import { CreateNativeLanguageInput } from './create-native-language.input';
declare const UpdateNativeLanguageInput_base: import("@nestjs/common").Type<Partial<CreateNativeLanguageInput>>;
export declare class UpdateNativeLanguageInput extends UpdateNativeLanguageInput_base {
    id: number;
}
export {};
