import { Language } from 'src/language/entities/language.entity';
import { User } from 'src/user/entities/user.entity';
export declare class NativeLanguage {
    id: number;
    user?: User;
    language?: Language;
}
